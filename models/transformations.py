from albumentations.core.composition import BaseCompose
from albumentations.core.transforms_interface import BasicTransform, DualTransform
import cv2
from albumentations.augmentations import functional as F
import numpy as np
import torch

class ToTensor(BasicTransform):
    """Convert image and mask to `torch.Tensor`.
    
    Multi-channel images will be returned as HxWxC by default, unless `permute_channels` is disabled.
    
    Masks will be returned with their input shape as a long tensor, unless `convert_one_hot` is enabled.

    Args:
        num_classes (int): only for segmentation
        convert_one_hot (bool): convert input labels to one-hot, only for segmentation, default False
        permute_channels (bool): whether to attempt to convert the image to HxWxC

    """

    def __init__(self, num_classes=1, convert_one_hot=False, permute_channels=True):
        super(ToTensor, self).__init__(always_apply=True, p=1.)
        self.num_classes = num_classes
        self.convert_one_hot = convert_one_hot
        self.permute_channels = permute_channels

    def __call__(self, force_apply=True, **kwargs):
        # Convert image to tensor
        kwargs.update({'image': self._image_to_tensor(kwargs['image'].astype(np.float32))})
        
        # Convert mask to tensor:
        if 'mask' in kwargs.keys():
            kwargs.update({'mask': self._mask_to_tensor(kwargs['mask'])})

        for k, v in kwargs.items():
            if self._additional_targets.get(k) == 'image':
                kwargs.update({k: self._image_to_tensor(kwargs[k])})
            if self._additional_targets.get(k) == 'mask':
                kwargs.update({k: self._mask_to_tensor(kwargs[k])})
        return kwargs
    
    def _image_to_tensor(self, image):
        """
        Convert input image to a tensor and permute colour channel if appropriate.
        """
        
        # Only permute if multi-channel
        if image.ndim > 2 and self.permute_channels:
            image = image.transpose(2,0,1)
        
        return torch.from_numpy(image)
        

    def _mask_to_tensor(self, mask):
        """
        Convert input mask into a long tensor.
        """
        mask_tensor = torch.from_numpy(mask).long()

        if self.num_classes > 1 and self.convert_one_hot:
            assert mask_tensor.ndim == 2
            mask_tensor = torch.nn.functional.one_hot(mask_tensor, self.num_classes).permute(2, 0, 1)

        return mask_tensor
    
    @property
    def targets(self):
        raise NotImplementedError

    def get_transform_init_args_names(self):
        return ('num_classes', 'convert_one_hot')

class PerChannel(BaseCompose):
    """Apply transformations per-channel

    Args:
        transforms (list): list of transformations to compose.
        channels (list): channels to apply the transform to. Pass None to apply to all.
                         Default: None (apply to all)
        p (float): probability of applying the transform. Default: 0.5.
    """

    def __init__(self, transforms, channels=None, p=0.5):
        super(PerChannel, self).__init__(transforms, p)
        self.transforms = transforms
        self.channels = channels

    def __call__(self, force_apply=False, **data):

        image = data['image']

        # Mono images
        if len(image.shape) == 2:
            image = np.expand_dims(image, -1)

        if self.channels is None:
            self.channels = range(image.shape[2])

        for c in self.channels:
            for t in self.transforms:
                image[:, :, c] = t(image=image[:, :, c])['image']

        data['image'] = image

        return data


class ResizeFactor(DualTransform):
    """Resize the input by the given factor (assume input (rows,cols,channels) or (rows,cols)

    Args:
        p (float): probability of applying the transform. Default: 1.
        downsampling_factor (int): desired height of the output.
        interpolation (OpenCV flag): flag that is used to specify the interpolation algorithm. Should be one of:
            cv2.INTER_NEAREST, cv2.INTER_LINEAR, cv2.INTER_CUBIC, cv2.INTER_AREA, cv2.INTER_LANCZOS4.
            Default: cv2.INTER_LINEAR.

    Targets:
        image, mask, bboxes

    Image types:
        uint8, float32
    """

    def __init__(self, downsampling_factor, interpolation=cv2.INTER_LINEAR, always_apply=True, p=1):
        super(ResizeFactor, self).__init__(always_apply, p)
        self.downsampling_factor = downsampling_factor
        self.interpolation = interpolation

    def apply(self, img, interpolation=cv2.INTER_LINEAR, **params):
        new_size = np.round(np.array(img.shape[:2])/self.downsampling_factor).astype(np.int64)
        return F.resize(img, height=new_size[0], width=new_size[1], interpolation=interpolation)

    def apply_to_bbox(self, bbox, **params):
        # Bounding box coordinates are scale invariant
        return bbox

    def get_transform_init_args_names(self):
        return ('height', 'width', 'interpolation')