import os
import rasterio
import rasterio.windows
from torch.utils.data import Dataset
import numpy as np
import random


BANDS_S2 = ["B1", "B2", "B3", "B4", "B5",
            "B6", "B7", "B8", "B8A", "B9",
            "B10", "B11", "B12"]

SENTINEL2_NORMALIZATION = np.array([
    [3787.0604973, 2634.44474043],
    [3758.07467509, 2794.09579088],
    [3238.08247208, 2549.4940614],
    [3418.90147615, 2811.78109878],
    [3450.23315812, 2776.93269704],
    [4030.94700446, 2632.13814197],
    [4164.17468251, 2657.43035126],
    [3981.96268494, 2500.47885249],
    [4226.74862547, 2589.29159887],
    [1868.29658114, 1820.90184704],
    [399.3878948,  761.3640411],
    [2391.66101119, 1500.02533014],
    [1790.32497137, 1241.9817628]], dtype=np.float32)


ELEVATION_NORMALIZATION = np.array([[181.65, 202.91]],
                                   dtype=np.float32)

SOURCES = {"CopernicusEMS", "unosat", "glofimr"}


# Brown, Blue, White colormap for ground truth
COLORS_WORLDFLOODS = np.array([[139, 64, 0],
                               [0, 0, 139],
                               [220, 220, 220]], dtype=np.float32) / 255


FREQUENCY_CLASSES = [0.120252 + 0.396639, 0.027322, .455787]

CHANNELS_CONFIGURATIONS = {
    'all': list(range(len(BANDS_S2))),
    'rgb': [1, 2, 3],
    'rgbi': [1, 2, 3, 7],
    'sub_20': [1, 2, 3, 4, 5, 6, 7, 8],
    'hyperscout2': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
}


class WorldFloodsDataset(Dataset):
    """
    A dataloader for the WorldFloods dataset.
    """
    def __init__(self, x_filenames,
                 window_size=(128, 128),
                 image_prefix='/S2/',
                 gt_prefix="/gt/",
                 transform=None,
                 limit=None,
                 sample=False,
                 yield_smaller_patches=False,
                 use_channels='all'):

        self.window_size = window_size
        self.image_prefix = image_prefix
        self.gt_prefix = gt_prefix
        self.transform = transform
        self.sample = sample
        self.last_filename = None
        self.yield_smaller_patches = yield_smaller_patches

        self.s2_channels = CHANNELS_CONFIGURATIONS[use_channels]

        self.x_filenames = list(x_filenames)

        # sort to make sure that the order is deterministic
        self.x_filenames.sort()

        for s2_full_path in self.x_filenames:

            y_name = s2_full_path.replace(self.image_prefix, self.gt_prefix, 1)

            if not os.path.exists(y_name):
                print("Mask GT does not exists for file %s" % s2_full_path)
                continue

            with rasterio.open(s2_full_path, "r") as src:
                shape = src.shape
            assert len(shape) == 2, "Unexpected shape {}".format(shape)

            # Discard images with shape smaller than window_size
            if (window_size is not None) and (not yield_smaller_patches) and ((shape[0] < window_size[0])
                                              or (shape[1] < window_size[1])):
                print("window_size (%d, %d) too big for file %s with shape (%d, %d). Image will be discarded" % (window_size[0],
                                                                                             window_size[1],
                                                                                             s2_full_path, shape[0], shape[1]))
                continue

        self.slices = None

        if (self.window_size is not None) and not self.sample:
            self._include_all(yield_smaller_patches)
            if limit is not None:
                vals = np.random.choice(len(self.x_filenames), size=limit, replace=False)
                self.x_filenames = [self.x_filenames[idx_v] for idx_v in vals]
                self.slices = [self.slices[idx_v] for idx_v in vals]

    def _include_all(self, yield_smaller_patches=False):
        """
        Add all the subimages of shape window_size
        :param yield_smaller_patches:
        :return:
        """
        new_x_filenames = []
        slices = []

        for full_x_name in self.x_filenames:
            assert os.path.exists(full_x_name), "File {} not found".format(full_x_name)
            shape_file = rasterio.open(full_x_name).shape
            filas_ = np.arange(0, shape_file[0], self.window_size[0])
            columnas_ = np.arange(0, shape_file[1], self.window_size[1])
            for r in filas_:
                for c in columnas_:
                    slice_ = (slice(r, min(r + self.window_size[0], shape_file[0])),
                              slice(c, min(c + self.window_size[1], shape_file[1])))

                    shape_slice = tuple([s.stop - s.start for s in slice_])
                    if shape_slice != self.window_size and not yield_smaller_patches:
                        continue
                    new_x_filenames.append(full_x_name)
                    slices.append(slice_)

        self.x_filenames = new_x_filenames
        self.slices = slices

    def __getitem__(self, idx):
        x_name = self.x_filenames[idx]

        if not os.path.exists(x_name):
            raise FileNotFoundError("File not found: {}".format(x_name))

        self.last_filename = self.x_filenames[idx]

        x_tif = rasterio.open(x_name)

        # Compute window to read
        if self.window_size is None:
            window = rasterio.windows.Window.from_slices(slice(0, x_tif.shape[0]),
                                                         slice(0, x_tif.shape[1]))
        elif self.sample:
            shape_file = x_tif.shape

            if not self.yield_smaller_patches and ((self.window_size[0] > shape_file[0]) or
                                                   (self.window_size[1] > shape_file[1])):
                raise RuntimeError("Specified window size is larger than image {}".format(x_name))

            # Window upper left
            if self.window_size[0] < shape_file[0]:
                r = random.randint(0, shape_file[0] - self.window_size[0])
            else:
                r = 0

            if self.window_size[1] < shape_file[1]:
                c = random.randint(0, shape_file[1] - self.window_size[1])
            else:
                c = 0

            slice_ = (slice(r, min(r + self.window_size[0], shape_file[0])),
                      slice(c, min(c + self.window_size[1], shape_file[1])))
            window = rasterio.windows.Window.from_slices(*slice_)
        else:
            slice_ = self.slices[idx]
            window = rasterio.windows.Window.from_slices(*slice_)

        # Read input
        channels_1_index_base_rasterio = [s+1 for s in self.s2_channels]
        x = x_tif.read(channels_1_index_base_rasterio, window=window)

        x = np.nan_to_num(x).astype(np.float32)  # get rid of nan, convert to float

        # Read from GT mask
        y_name = x_name.replace(self.image_prefix, self.gt_prefix, 1)
        if not os.path.exists(y_name):
            raise FileNotFoundError("No corresponding label found for file {}".format(x_name))
        y_tif = rasterio.open(y_name)
        if not x_tif.bounds == y_tif.bounds:
            raise RuntimeError("Bounds for tif files {} and {} do not match".format(x_name, y_name))

        # gt values {0: invalid, 1: land, 2: water, 3: cloud}
        y_gt = y_tif.read(window=window)

        y = np.nan_to_num(y_gt[0])

        # Apply transformation
        if self.transform is not None:
            res = self.transform(image=x.transpose(1, 2, 0),
                                 mask=y)
            x, y = res['image'], res['mask']

        return x, y

    def __len__(self):
        return len(self.x_filenames)

